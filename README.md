Readme
================================================================================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
================================================================================
This module helps to add subscription option to any node. After installing the
module user can subscribe to individual nodes for email notification
when a new comment is added.


Features Include:

    1.  Creates a Block that can be added to any content type
    2.  Users can subscribe to any content type
    3.  Both subscribe and unsubscribe option is available
    4.  Email notification when new comment is added


REQUIREMENTS
================================================================================
Drupal 8.x


INSTALLATION
================================================================================
1. Download module
2. Enable module using UI or 'drush en node_comment_subscribe' command
3. Goto Structure -> Block Layout -> Place Block
4. Search for 'Subscribe/Unsubscribe To Comments Here' and Click 'Place Block'
5. Add 'Title' and Select Content Type/Types where you want the option to show
6. Click 'Save Block'.

CONFIGURATION
================================================================================
No configuration page has been created for this module.
