<?php

/**
 * @file
 * Node Comment Subscribe module install file.
 */

/**
 * Implements hook_schema().
 */
function node_comment_subscribe_schema(): array {
  $schema['node_comment_subscribe'] = [
    'description' => 'Contains details of user subscription for any node.',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for a row.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'user_id' => [
        'description' => 'userid of logged in user.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ],
      'timestamp' => [
        'description' => 'A Unix timestamp indicating when this row was created.',
        'mysql_type' => 'timestamp',
        'not null' => TRUE,
      ],
      'node_id' => [
        'description' => 'Node ID.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'subscribed' => [
        'description' => 'Subscribed',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
    ],
    'primary key' => ['id'],
  ];
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function node_comment_subscribe_uninstall() {
  \Drupal::configFactory()->getEditable('node_comment_subscribe.settings')->delete();
}
