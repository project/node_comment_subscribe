<?php

namespace Drupal\node_comment_subscribe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a subscribe form.
 */
class SubscribeForm extends FormBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Add constructor function.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database Connection.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route Match.
   */
  public function __construct(Connection $database, RouteMatchInterface $route_match) {
    $this->database = $database;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->checkIfRecordExixst() == "Subscribed") {
      $form['subscribe_user'] = [
        '#type' => 'checkboxes',
        '#options' => [
          'subscribe' => $this->t('Subscribe to new comments for this content'),
        ],
        '#default_value' => [
          'subscribe',
        ],
      ];
    }
    else {
      $form['subscribe_user'] = [
        '#type' => 'checkboxes',
        '#options' => [
          'subscribe' => $this->t('Subscribe to new comments for this content'),
        ],
      ];
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Subscribe/Un-Subscribe'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->currentUser();
    $uid = $user->id();
    $node = $this->routeMatch->getParameter('node');
    $nid = $node->nid->value;
    $subscribe = 0;
    if ($form_state->getValue('subscribe_user')['subscribe'] === 'subscribe') {
      $subscribe = 1;
    }

    if ($this->checkIfRecordExixst() == "Not Found") {
      $table_values = [
        [
          'user_id' => $uid,
          'node_id' => $nid,
          'subscribed' => $subscribe,
        ],
      ];
      $query = $this->database->insert('node_comment_subscribe')
        ->fields([
          'user_id',
          'node_id',
          'subscribed',
        ]);
      foreach ($table_values as $record) {
        $query->values($record);
      }
      $query->execute();
      $message = $this->messenger();
      if ($subscribe == 1) {
        $message->addMessage($this->t('Thank you for your subscription. You will now get email when new comment/reply is added to this content.'));
      }
      else {
        $message->addMessage($this->t('Please check "Subscribe to new comments for this content" before submitting.'));
      }
    }
    else {
      $table_values = [
        [
          'subscribed' => $subscribe,
        ],
      ];
      $query = $this->database->update('node_comment_subscribe')
        ->fields([
          'subscribed' => $subscribe,
        ])
        ->condition('user_id', $uid, '=')
        ->condition('node_id', $nid, '=');
      $query->execute();
      $message = $this->messenger();
      if ($subscribe == 0) {
        $message->addMessage($this->t('You have successfully unsubscribed for comment notification on this content.'));
      }
      else {
        $message->addMessage($this->t('You have successfully subscribed for email notification on new comment for this content.'));
      }

    }
  }

  /**
   * Check if the record for subscription already exists.
   */
  private function checkIfRecordExixst() {
    $user = $this->currentUser();
    $uid = $user->id();
    $node = $this->routeMatch->getParameter('node');
    $nid = $node->nid->value;
    $query = $this->database->select('node_comment_subscribe', 'ncs')
      ->condition('user_id', $uid, '=')
      ->condition('node_id', $nid, '=')
      ->fields('ncs', ['id', 'subscribed'])
      ->range(0, 1);
    $result = $query->execute();
    $found = "Not Found";
    foreach ($result as $record) {
      if ($record->subscribed == 0) {
        $found = "Not Subscribed";
      }
      else {
        $found = "Subscribed";
      }
    }
    return $found;
  }

}
